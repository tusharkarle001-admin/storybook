import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { Story, Meta } from '@storybook/angular';
import { ImgesComponent } from 'src/app/imges/imges.component';
export default {
  title: 'Example/Image Component',
  component: ImgesComponent,
} as Meta;

const Template: Story<ImgesComponent> = (args: ImgesComponent) => ({
  Component: ImgesComponent, //take img compoent
  props: args, //take all the properties of arguments
});

// for the noCaption Story

// use the template to bind the properties given by args
// send the args here
export const noImageCaption = Template.bind({});

noImageCaption.args = {
  figCaptionTxt: '',
};

// for with Caption stroy
export const WithImgCaption = Template.bind({});
WithImgCaption.args = {
  figCaptionTxt: 'The caption description',
};

// for halfopacity
export const withHalfOpacity = Template.bind({});
withHalfOpacity.args = { imgOpacity: 0.5 };

// for full opacity
export const withFullOpacity = Template.bind({});
withFullOpacity.args = { imgOpacity: 1 };
