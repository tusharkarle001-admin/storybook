import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { Story, Meta } from '@storybook/angular';
import { CustomButtonComponent } from 'src/app/custom-button/custom-button.component';
export default {
	title: 'Example/Custom-button',
	component: CustomButtonComponent,
} as Meta;

const Template: Story<CustomButtonComponent> = (args: CustomButtonComponent) => ({
	Component: CustomButtonComponent,
	props: args,
});

export const primaryButton = Template.bind({});
primaryButton.args = {
	title: 'Primary Button',
	buttonFontSize: '10px',
	primaryButton: true,
	displayIcon: true,
	roundedButton: false,
	iconLink: '../../assets/perm_identity_white_24dp.svg',
	iconRight: false,
};
