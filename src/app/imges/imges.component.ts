import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-imges',
  templateUrl: './imges.component.html',
  styleUrls: ['./imges.component.scss'],
})
export class ImgesComponent implements OnInit {
  @Input() imgSrc: string = '../../assets/istockphoto-1093110112-612x612.jpg';
  @Input() altText?: string = 'Pondechery French hotel';
  @Input() figCaptionTxt?: string = 'Hello description';
  @Input() imgOpacity?: number = 1;

  constructor() {}

  ngOnInit(): void {}
}
